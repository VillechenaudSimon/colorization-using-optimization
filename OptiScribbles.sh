#!/bin/bash

# This script aims to execute the Colorization Using Optimization method on the given images (WIP)

# $1 Image name 
# $2 original images folder path
# $3 scribbles folder path
# $4 Image result absolute path

virtualenv venv --python=python3
source venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt

for image in $1
do
    python3 colorize.py --original_image $2/$image --visual_clue $3/$image --result_path $4/${image%.*}  
done

deactivate
exit 0 